/*
 * Copyright (C) 2017, Christopher F. Moran
 * 
 * This app displays large random numbers on a series of 8x8 displays
 * Using the MAX7219 chip
 * 
 * D16 = Data IN on first MAX7219
 * D15 = CLK to all MAX7219 devices
 * D14 = LOAD to all MAX7219 devices
 * 
 * For subsequent devices, connect DATA OUT from previous to DATA IN on the current MAX7219
 * 
 */
#include <LedControl.h>

#define NUM_DISP  4

 LedControl lc1 = LedControl(16, 15, 14, NUM_DISP);

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  int i = 0;

  randomSeed(analogRead(0));

  // Wake up the controller
  for (i = 0; i < NUM_DISP; i++) {
    lc1.shutdown(i, false);
    lc1.setIntensity(i,8);
  }
  digitalWrite(LED_BUILTIN, HIGH);
  lc1.setColumn(0, 7, B10000001);
  lc1.setColumn(0, 6, B10000001);
  lc1.setColumn(0, 5, B10000001);
  lc1.setColumn(0, 4, B10000001);
  lc1.setColumn(0, 3, B10000001);
  lc1.setColumn(0, 2, B01000010);
  lc1.setColumn(0, 1, B00100100);
  lc1.setColumn(0, 0, B00011000);

  lc1.setColumn(1, 7, B00011000);
  lc1.setColumn(1, 6, B01111000);
  lc1.setColumn(1, 5, B00011000);
  lc1.setColumn(1, 4, B00011000);
  lc1.setColumn(1, 3, B00011000);
  lc1.setColumn(1, 2, B00011000);
  lc1.setColumn(1, 1, B00011000);
  lc1.setColumn(1, 0, B11111111);

  lc1.setColumn(2, 7, B00000000);
  lc1.setColumn(2, 6, B00000000);
  lc1.setColumn(2, 5, B00000000);
  lc1.setColumn(2, 4, B00000000);
  lc1.setColumn(2, 3, B00000000);
  lc1.setColumn(2, 2, B00000000);
  lc1.setColumn(2, 1, B00011000);
  lc1.setColumn(2, 0, B00011000);

  lc1.setColumn(3, 7, B00011000);
  lc1.setColumn(3, 6, B01111000);
  lc1.setColumn(3, 5, B00011000);
  lc1.setColumn(3, 4, B00011000);
  lc1.setColumn(3, 3, B00011000);
  lc1.setColumn(3, 2, B00011000);
  lc1.setColumn(3, 1, B00011000);
  lc1.setColumn(3, 0, B11111111);

  for(i = 8; i >= 0; i--) {
    for (int j = 0; j < NUM_DISP; j++) {
      lc1.setIntensity(j,i);
    }
    delay(500);
  }
  for (i = 0; i < NUM_DISP; i++)
    clearDisplay(i);
  digitalWrite(LED_BUILTIN, LOW);
  for(i = 0; i < NUM_DISP; i++) {
    lc1.setIntensity(i,6);
  }
}

void easterEgg() {
  lc1.setColumn(0, 7, B00011000);
  lc1.setColumn(0, 6, B00100100);
  lc1.setColumn(0, 5, B01000010);
  lc1.setColumn(0, 4, B10000001);
  lc1.setColumn(0, 3, B11111111);
  lc1.setColumn(0, 2, B10000001);
  lc1.setColumn(0, 1, B10000001);
  lc1.setColumn(0, 0, B00000000);
  delay(500);
  lc1.setColumn(0, 7, B00000000);
  lc1.setColumn(0, 6, B01101100);
  lc1.setColumn(0, 5, B10010010);
  lc1.setColumn(0, 4, B10010010);
  lc1.setColumn(0, 3, B10010010);
  lc1.setColumn(0, 2, B10010010);
  lc1.setColumn(0, 1, B10010010);
  lc1.setColumn(0, 0, B00000000);
  delay(500);
  lc1.setColumn(0, 7, B00000000);
  lc1.setColumn(0, 6, B01111100);
  lc1.setColumn(0, 5, B10000010);
  lc1.setColumn(0, 4, B10000010);
  lc1.setColumn(0, 3, B11111100);
  lc1.setColumn(0, 2, B10000000);
  lc1.setColumn(0, 1, B01111100);
  lc1.setColumn(0, 0, B00000000);
  delay(500);
  lc1.setColumn(0, 7, B00011000);
  lc1.setColumn(0, 6 ,B00011000);
  lc1.setColumn(0, 5, B00011000);
  lc1.setColumn(0, 4, B00011000);
  lc1.setColumn(0, 3, B00011000);
  lc1.setColumn(0, 2, B00011000);
  lc1.setColumn(0, 1, B00011000);
  lc1.setColumn(0, 0, B00000000);
  delay(500);
  lc1.setColumn(0, 7, B00000000);
  lc1.setColumn(0, 6, B00011000);
  lc1.setColumn(0, 5, B00000000);
  lc1.setColumn(0, 4, B00011000);
  lc1.setColumn(0, 3, B00011000);
  lc1.setColumn(0, 2, B00011000);
  lc1.setColumn(0, 1, B00011000);
  lc1.setColumn(0, 0, B00000000);
  delay(500);
  lc1.setColumn(0, 7, B00000000);
  lc1.setColumn(0, 6, B01111110);
  lc1.setColumn(0, 5, B00000010);
  lc1.setColumn(0, 4, B01111110);
  lc1.setColumn(0, 3, B10000010);
  lc1.setColumn(0, 2, B10000010);
  lc1.setColumn(0, 1, B01111100);
  lc1.setColumn(0, 0, B00000000);
  delay(500);
  clearDisplay(0);
}

void clearDisplay(int id) {
  lc1.setColumn(id, 0, 0);
  lc1.setColumn(id, 1, 0);
  lc1.setColumn(id, 2, 0);
  lc1.setColumn(id, 3, 0);
  lc1.setColumn(id, 4, 0);
  lc1.setColumn(id, 5, 0);
  lc1.setColumn(id, 6, 0);
  lc1.setColumn(id, 7, 0);
}

byte getBrightness() {
  return map(analogRead(A0), 0, 8, 0, 1024);
}

void loop() {
  unsigned long val = random(0, pow(2, 8 * NUM_DISP));
  if(random(0, 500) % 2 == 0) {
    val = val * 2;
  }
  byte disp = random(0, 8);
  byte brightness = getBrightness();
  lc1.setIntensity(0, brightness);
  lc1.setIntensity(1, brightness);
  lc1.setIntensity(2, brightness);
  lc1.setIntensity(3, brightness);
  lc1.setColumn(0, disp, ((val >> 24) & 0x000000ff));
  lc1.setColumn(1, disp, ((val >> 16) & 0x000000ff));
  lc1.setColumn(2, disp, ((val >> 8) & 0x000000ff));
  lc1.setColumn(3, disp, (val & 0x000000ff));
  delay(random(0, 375));
}

