# FlashyBox #

## Useful for ##
* Are you an ageing sysprog who pines for simpler times when you could tell what your machines were doing by looking at the front panel?
* Are you a Data Centre or Infrastructure Manager with no machines left to impress people with?
* Do you just want a clever-looking toy for your desk?
* See a demo at: https://youtu.be/pxeaGnFxQqM

Then FlashyBox is for you.

FlashyBox is completely useless, but done right it looks something like the front panels which real computers once had.  It can be driven by the smallest of Arduino boards, because it needs only 3 I/O lines.  If you wanted to get really clever, you could pull down market prices from Google or Bing and display those, but it's just as cute doing random numbers.

FlashyBox depends on the LEDControl library, which can be installed via the Arduino IDE.